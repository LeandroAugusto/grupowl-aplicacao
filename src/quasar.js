import Vue from "vue";

import "./styles/quasar.styl";
import "@quasar/extras/roboto-font/roboto-font.css";
import lang from "quasar/lang/pt-br.js";
import "@quasar/extras/material-icons/material-icons.css";
import "@quasar/extras/fontawesome-v5/fontawesome-v5.css";
import {
  Quasar,
  QLayout,
  QHeader,
  QDrawer,
  QPageContainer,
  QSpace,
  QSeparator,
  QBar,
  QTree,
  QPage,
  Loading,
  QTooltip,
  QBadge,
  QPopupProxy,
  QRadio,
  ClosePopup,
  QCheckbox,
  QExpansionItem,
  QToolbar,
  QTable,
  QTh,
  QTr,
  QTd,
  QBtnGroup,
  QDialog,
  QToolbarTitle,
  QParallax,
  QBtn,
  QBtnDropdown,
  Dialog,
  QIcon,
  QList,
  QItem,
  QItemSection,
  QItemLabel
} from "quasar";

Vue.use(Quasar, {
  config: {
    loading: {}
  },
  components: {
    QLayout,
    QHeader,
    QDrawer,
    Loading,
    QPageContainer,
    QDialog,
    QBar,
    QTree,
    QPage,
    QBtnDropdown,
    QParallax,
    QExpansionItem,
    QToolbar,
    Dialog,
    QBtnGroup,
    QTable,
    QSeparator,
    QTh,
    QTr,
    QTd,
    QDate,
    QPopupProxy,
    QRadio,
    QTooltip,
    ClosePopup,
    QToolbarTitle,
    QSpace,
    QBadge,
    QCheckbox,
    QBtn,
    QIcon,
    QList,
    QItem,
    QItemSection,
    QItemLabel
  },
  directives: {
    ClosePopup
  },
  plugins: {
    Loading,
    Dialog
  },
  lang: lang
});
