import axios from "axios";
import mascara from "@/commons/mascara.js";

const URL_API = process.env.VUE_APP_URL_API;

export default {
  methods: {
    buscarUsuario(filtro) {
      return axios.get(URL_API + "usuario/buscar", {
        params: {
          nome: filtro.nome,
          cpf: filtro.cpf,
        },
      });
    },

    salvarUsuario(dados) {
      return axios.post(URL_API + "usuario/salvar-usuario", {
        nome: dados.nome,
        cpf: dados.cpf,
        dataNascimento: mascara.methods.localDate(dados.dataNascimento),
      });
    },

    atualizarUsuario(dados) {
      return axios.put(URL_API + "usuario/atualizar-usuario/" + dados.id, {
        nome: dados.nome,
        cpf: dados.cpf,
        dataNascimento: mascara.methods.localDate(dados.dataNascimento),
      });
    },

    excluirUsuario(id) {
      return axios.delete(URL_API + "usuario/excluir-usuario/" + id);
    },
  },
};
