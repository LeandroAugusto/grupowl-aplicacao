import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./styles/quasar.styl";
import "@quasar/extras/material-icons/material-icons.css";
import "@quasar/extras/fontawesome-v5/fontawesome-v5.css";
import PdPeriodo from "./components/Pd-Periodo.vue";
import PdDate from "./components/Pd-Date.vue";
import PdMoney from "./components/Pd-Money.vue";
import PdCpfCnpj from "./components/Pd-CpfCnpj.vue";
import VueApexCharts from "vue-apexcharts";
import money from "v-money";
import langDe from "quasar/lang/pt-br.js";

import {
  Quasar,
  QBreadcrumbs,
  QBreadcrumbsEl,
  QCheckbox,
  QDate,
  QPopupProxy,
  QAvatar,
  QLayout,
  QSelect,
  QHeader,
  QBadge,
  QFooter,
  QMenu,
  QDrawer,
  QPageContainer,
  QPage,
  QSpace,
  QToolbar,
  QRadio,
  QScrollArea,
  QDialog,
  ClosePopup,
  QToolbarTitle,
  QBtn,
  QField,
  QBar,
  QBtnGroup,
  QIcon,
  QList,
  QItem,
  QItemSection,
  QItemLabel,
  QInput,
  QTabPanels,
  QExpansionItem,
  QBtnDropdown,
  QTabs,
  Loading,
  Dialog,
  QTab,
  QTooltip,
  QRouteTab,
  QTabPanel,
  QStepper,
  QTree,
  QStep,
  QStepperNavigation,
  QCard,
  QCardSection,
  QCardActions,
  QParallax,
  QSeparator,
  QLinearProgress,
  QToggle,
  QTable,
  QTh,
  QTr,
  QTd,
  QForm,
  Notify
} from "quasar";

Vue.component("apexchart", VueApexCharts);
Vue.component("pd-periodo", PdPeriodo);
Vue.component("pd-date", PdDate);
Vue.component("pd-money", PdMoney);
Vue.component("pd-cpf-cnpj", PdCpfCnpj);
Vue.use(money, { precision: 4 });

Vue.use(Quasar, {
  config: {
    Loading
  },
  lang: langDe,
  components: {
    QLayout,
    QBreadcrumbs,
    QBreadcrumbsEl,
    QAvatar,
    QSelect,
    QField,
    QDate,
    QTabs,
    QTab,
    QRouteTab,
    QBar,
    QExpansionItem,
    QSeparator,
    QTree,
    QScrollArea,
    QHeader,
    QFooter,
    QBtnDropdown,
    QCheckbox,
    QRadio,
    QParallax,
    QTabPanels,
    QBadge,
    QTabPanel,
    QMenu,
    QPopupProxy,
    QDrawer,
    QSpace,
    QBtnGroup,
    QPageContainer,
    QPage,
    QToolbar,
    QToolbarTitle,
    QBtn,
    Loading,
    QDialog,
    QIcon,
    QList,
    QItem,
    QItemSection,
    QTooltip,
    QItemLabel,
    QToggle,
    QInput,
    Dialog,
    QStepper,
    QStep,
    QStepperNavigation,
    QCard,
    QCardSection,
    QCardActions,
    QLinearProgress,
    QTable,
    QTh,
    QTr,
    QTd,
    QForm
  },
  directives: {
    ClosePopup
  },
  plugins: {
    Notify,
    Loading,
    Dialog
  }
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
