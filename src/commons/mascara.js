import { date } from "quasar";
import moment from "moment";

export default {
  data() {
    return {
      tableVisible: false
    };
  },

  methods: {
    cpfCnpj(valor) {
      if (valor.length <= 11) {
        valor = ("00000000000" + valor).slice(-11);
        return valor.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "$1.$2.$3-$4");
      }
      valor = ("00000000000000" + valor).slice(-14);
      return valor.replace(
        /(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,
        "$1.$2.$3/$4-$5"
      );
    },

    valorMonetario(valor) {
      let val = (valor / 1).toFixed(2).replace(".", ",");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    },

    telefone(valor) {
      let val = valor.replace(/\-/g, "");
      const p1 = valor.slice(0, 2);
      const p2 = valor.slice(2, 7);
      const p3 = valor.slice(7, 11);
      val = `(${p1}) ${p2}-${p3}`;
      return val;
    },

    data(valor) {
      return moment(valor).format("DD/MM/YYYY");
    },

    localDate(valor) {
      return moment(valor).format("YYYY-MM-DD");
    }
  }
};
