import { Notify } from "quasar";

Notify.setDefaults({
  position: "top-right",
  timeout: 10000,
  textColor: "white",
});

export default {
  name: "notificacoes-principal",
  data() {
    return {};
  },
  methods: {
    alert(mensagem) {
      Notify.create({
        message: mensagem,
        color: "yellow",
        textColor: "black",
        icon: "warning",
        actions: [
          {
            icon: "close",
            color: "black",
          },
        ],
      });
    },

    error(mensagem) {
      Notify.create({
        message: mensagem,
        color: "red",
        icon: "fas fa-skull-crossbones",
        actions: [
          {
            icon: "close",
            color: "white",
          },
        ],
      });
    },

    info(mensagem) {
      Notify.create({
        message: mensagem,
        color: "blue",
        icon: "info",
        actions: [
          {
            icon: "close",
            color: "white",
          },
        ],
      });
    },

    success(mensagem) {
      Notify.create({
        message: mensagem,
        color: "green",
        icon: "done",
        actions: [
          {
            icon: "close",
            color: "white",
          },
        ],
      });
    },
  },
};
