import notificacao from "@/commons/notificacao.js";
import mascara from "@/commons/mascara.js";
import Usuario from "@/classes/Usuario";
import service from "@/service/usuarioService.js";

export default {
  mixins: [notificacao, mascara, service],
  data() {
    return {
      usuario: new Usuario(),
      filtro: {
        nome: null,
        cpf: null
      },
      filter: "",
      tableVisivel: false,
      columns: [
        {
          name: "nome",
          field: "nome",
          label: "NOME USUÁRIO",
          style: "font-size: 11px;",
          align: "center",
          sorteble: true
        },
        {
          name: "cpf",
          align: "center",
          label: "CPF",
          field: row => this.cpfCnpj(row.cpf),
          style: "font-size: 11px;",
          sorteble: true
        },
        {
          name: "dataNascimento",
          label: "DATA NASCIMENTO",
          field: row => this.data(row.dataNascimento),
          align: "center",
          style: "font-size: 11px;",
          sorteble: true
        },
        {
          name: "acoes",
          field: "acoes",
          label: "AÇÕES",
          align: "center"
        }
      ],
      listaUsuario: [{}]
    };
  },

  mounted() {},

  methods: {
    buscaUsuario() {
      this.$q.loading.show({ message: "Carregando..." });
      this.buscarUsuario(this.filtro).then(response => {
        this.listaUsuario = response.data;
        if (this.listaUsuario != 0) {
          this.tableVisivel = true;
        } else {
          this.info("Nenhum registro encontrado");
          this.tableVisivel = false;
        }
        this.$q.loading.hide();
      });
    },

    excluir(linha) {
      this.$q
        .dialog({
          icon: "far fa-trash-alt",
          title: "Exclusão de Registro",
          message: "Deseja Realmente Excluir o Registro?",
          cancel: {
            push: true,
            size: "10px",
            label: "Cancelar",
            icon: "cancel",
            color: "negative"
          },
          ok: {
            push: true,
            label: "Confirmar",
            icon: "done",
            size: "10px",
            color: "primary"
          },
          persistent: true
        })
        .onOk(() => {
          this.$q.loading.show({ message: "Excluindo Registro..." });
          this.excluirUsuario(linha.id)
            .then(() => {
              for (var i in this.listaUsuario) {
                if (this.listaUsuario[i] === linha) {
                  this.listaUsuario.splice(i, 1);
                  break;
                }
              }
              this.success("Registro excluído com sucesso!");
              this.$q.loading.hide();
            })
            .catch(error => {
              this.error("Falha ao excluir registro!");
              this.$q.loading.hide();
              console.log(error);
            });
        })
        .onCancel(() => {});
    },

    limpar() {
      for (let i in this.filtro) {
        this.filtro[i] = null;
      }
      this.listaUsuario = [];
      this.tableVisivel = false;
    }
  }
};
