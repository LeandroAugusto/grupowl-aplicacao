import Vue from "vue";
import VueRouter from "vue-router";

import DefaultLayout from "../layouts/Layout-Default.vue";
import Menu from "../layouts/Menu.vue";
import Usuario from "../views/Usuario.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: DefaultLayout,
    children: [
      {
        path: "/",
        name: "usuario",
        component: Usuario,
      },
      {
        path: "/form-usuario:usuario",
        name: "formUsuario",
        component: () => import("@/views/forms/Form-Usuario.vue"),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
